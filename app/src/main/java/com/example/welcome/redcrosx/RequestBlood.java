package com.example.welcome.redcrosx;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.welcome.redcrosx.RecyclerView.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestBlood extends AppCompatActivity {
    TextView name;
    TextView contact;
    String name1="";
    TextView donations;
    Context c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_blood);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width= dm.widthPixels;
        int height= dm.heightPixels;
        c=this;
        getWindow().setLayout(width,height);
        name=(TextView)findViewById(R.id.name);
        contact=(TextView)findViewById(R.id.contact);
        donations=(TextView)findViewById(R.id.donation);
        final String donorId= getIntent().getStringExtra("DonorId");
        System.out.println("donnor"+donorId);
        DatabaseReference dbr= FirebaseDatabase.getInstance().getReference().child("users").child(donorId);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    User user= dataSnapshot.getValue(User.class);

                    System.out.println("user"+user.getEmail());
                    if(dataSnapshot.child("fName")!=null){
                        name1+=dataSnapshot.child("fName").getValue(String.class);
                    }if(dataSnapshot.child("lname")!=null){
                        name1+=dataSnapshot.child("lname").getValue(String.class);
                        name.setText(name1);
                    }

                    if(dataSnapshot.child("phoneNumber")!=null){
                        contact.setText(dataSnapshot.child("phoneNumber").getValue(String.class));
                    }
                    else {
                        contact.setText(user.getEmail());
                    }
                    if(dataSnapshot.child("donation")!=null){
                        donations.setText(dataSnapshot.child("donation").getValue(String.class));
                    }
                    else{
                        donations.setText("0");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final EditText dateodappointment=(EditText) findViewById(R.id.appointmentDate);
        dateodappointment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(dateodappointment.getText()==null){
                    dateodappointment.setError("date must not be null");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Button Done= findViewById(R.id.requestBlood);
        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String donorId= getIntent().getStringExtra("DonorId");
                String userId= FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference m= FirebaseDatabase.getInstance().getReference().child("notification").child(donorId);
                Notification mynotify= new Notification();
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                Date date = new Date();
                System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
                mynotify.setNotificationDate(dateFormat.format(date));
                String message=FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has requested blood from You";
                mynotify.setMessage(message);
                mynotify.setReciepientId(userId);
                mynotify.setRecipentName(name1);
                mynotify.setAppointment(dateodappointment.getText().toString());
                mynotify.setFlag(0);
                m.push().setValue(mynotify);
                LayoutInflater factory = LayoutInflater.from(c);
                final View deleteDialogView = factory.inflate(R.layout.mylayout, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(c).create();
                deleteDialog.setView(deleteDialogView);
                deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //your business logic
                        deleteDialog.dismiss();
                    }
                });

                deleteDialog.show();

                Intent ne=new Intent(c, MapsActivity.class);
                startActivity(ne);
            }
        });
    }

}

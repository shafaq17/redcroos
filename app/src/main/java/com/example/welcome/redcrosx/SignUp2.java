package com.example.welcome.redcrosx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignUp2 extends AppCompatActivity {
public EditText fname;
public EditText lname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up2);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar3);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);
         fname=(EditText)findViewById(R.id.fname);
         lname=(EditText)findViewById(R.id.lname);
        Button button = (Button) findViewById(R.id.SignUpbtn2);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent myIntent = new Intent(SignUp2.this, SignUp3.class);
                String firstName=fname.getText().toString();
                String lastName=lname.getText().toString();
                if(firstName.matches("")){
                    fname.setError("required");
                }
                else if(lastName.matches("")){
                    lname.setError("required");
                }
                else {
                    Log.e("error",firstName);
                    String email=getIntent().getStringExtra("email");
                    String pass=getIntent().getStringExtra("password");
                    User a = new User();
                    a.setEmail(email);
                    a.setfName(fname.getText().toString());
                    a.setLname(lname.getText().toString());
                    a.setPassword(pass);
                    myIntent.putExtra("fname", fname.getText().toString());
                    myIntent.putExtra("lname", lname.getText().toString());
                    myIntent.putExtra("email1", email);
                    myIntent.putExtra("password1", pass);
                    myIntent.putExtra("user", a);
                    //("hel",email+" hi " + pass);
                    System.out.println(email+" hi " + pass);
                    startActivity(myIntent);
                }
            }
        });
    }
}

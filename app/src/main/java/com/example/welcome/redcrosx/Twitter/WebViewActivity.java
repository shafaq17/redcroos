package com.example.welcome.redcrosx.Twitter;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.welcome.redcrosx.R;

public class WebViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        String username = getIntent().getStringExtra("username");
        TextView uname = (TextView)findViewById(R.id.textView18);
        uname.setText(username);
    }
}

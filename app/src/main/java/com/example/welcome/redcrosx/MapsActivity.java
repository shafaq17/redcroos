package com.example.welcome.redcrosx;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener,com.google.android.gms.location.LocationListener  {
    Context c;
    private GoogleMap mMap;
    private LocationManager mLocationManager = null;
    private String provider = null;
    private Marker mCurrentPosition = null;
    Location mlastLocation;
    ImageButton menu;
Marker myMarker;

    private FusedLocationProviderClient mFusedLocationClient;
    private LatLng pickupLocation;
    private Marker pickupMarker;
    private int radius = 1;
    private Boolean donorFound = false;
    private String donorFoundID;
    GeoQuery geoQuery;
    private LocationRequest mLocationRequest;
    private FirebaseAnalytics mFirebaseAnalytics;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //startService(new Intent(this, TrackDonors.class));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        System.out.println("dont1");
        c=this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        final Button find_donor = (Button) findViewById(R.id.Find);
        find_donor.bringToFront();
        find_donor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("customerRequest");
                GeoFire geoFire = new GeoFire(ref);
                geoFire.setLocation(userId, new GeoLocation(mlastLocation.getLatitude(), mlastLocation.getLongitude()));

                pickupLocation = new LatLng(mlastLocation.getLatitude(), mlastLocation.getLongitude());
                pickupMarker = mMap.addMarker(new MarkerOptions().position(pickupLocation).title("Donate Here"));
                DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
                connectedRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        boolean connected = snapshot.getValue(Boolean.class);
                        if (connected) {
                            find_donor.setText("Finding your Donor....");

                            getClosestDriver();
                        } else {
                            Toast.makeText(c,"Network not connected",Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        System.err.println("Listener was cancelled");
                    }
                });

            }
        });


        SearchView search=(SearchView) findViewById(R.id.search);
        search.bringToFront();

        menu=(ImageButton) findViewById(R.id.menu);
        menu.bringToFront();

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuBuilder menuBuilder = new MenuBuilder(MapsActivity.this);
                MenuInflater inflater=new MenuInflater(MapsActivity.this);
                inflater.inflate(R.menu.menu_popup,menuBuilder);
                MenuPopupHelper popupmenu=new MenuPopupHelper(MapsActivity.this,menuBuilder,view);
                popupmenu.setForceShowIcon(true);

                menuBuilder.setCallback(new MenuBuilder.Callback() {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.Profile:
                                Toast.makeText(MapsActivity.this,""+ item.getTitle(),Toast.LENGTH_SHORT).show();
                                Intent myIntent = new Intent(MapsActivity.this, ProfileTabs.class);
                                startActivity(myIntent);
                                return true;
                            case R.id.Appointment:
                                Toast.makeText(MapsActivity.this,""+ item.getTitle(),Toast.LENGTH_SHORT).show();
                                Intent myIntent2 = new Intent(MapsActivity.this, ViewAppointments.class);
                                startActivity(myIntent2);
                                return true;
                            case R.id.Logout:
                                FirebaseAuth.getInstance().signOut();
                                Intent i= new Intent(c, SignUp.class);
                                startActivity(i);
                                return true;
                            case R.id.Register:
                                Intent intentOfMine=new Intent(c,MyLocationService.class);
                                startService(
                                        intentOfMine);
                                Bundle bundle = new Bundle();
                                bundle.putString("userEmail", FirebaseAuth.getInstance().getCurrentUser().getEmail());
                                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                                Date date = new Date();
                                bundle.putString("dateOfRegister", dateFormat.format(date));
                                mFirebaseAnalytics.logEvent("Registered_Donors", bundle);

                                Toast.makeText(MapsActivity.this,"Registered succesfully",Toast.LENGTH_SHORT).show();

                                return true;
                            default:
                                return false;
                        }
                    }

                    @Override
                    public void onMenuModeChange(MenuBuilder menu) {

                    }
                });
                popupmenu.show();
            }
        });

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    private void getClosestDriver(){
        final DatabaseReference donorLocation = FirebaseDatabase.getInstance().getReference().child("DonorsAvalible");

        GeoFire geoFire = new GeoFire(donorLocation);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(pickupLocation.latitude, pickupLocation.longitude), radius);
        geoQuery.removeAllListeners();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                if (!donorFound ){
                    DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(key);
                    System.out.println("key is"+key);
                    mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                          {
                                System.out.println("he;lk");
                                if(donorFound){
                                    return;
                                }
                                Map<String, Object> driverMap = (Map<String, Object>) dataSnapshot.getValue();

                                donorFound=true;
                                donorFoundID=dataSnapshot.getKey();

                                    DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("customerRequest").child(donorFoundID);
                                    String customerId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    HashMap map = new HashMap();
                                    map.put("customerRideId", customerId);
                                    User user= dataSnapshot.getValue(User.class);

                                if(dataSnapshot.child("lname")!=null){
                                    map.put("donnorLastName",user.getLname());
                                }
                                if(dataSnapshot.child("email")!=null){
                                    map.put("Donnoremail",user.getEmail());
                                }
                               if(dataSnapshot.child("fName")!=null){
                                   map.put("donnorName",user.getfName());
                               }
                               map.put("status","Not responded yet");
                                driverRef.updateChildren(map);
                                donorLocation. child(donorFoundID).removeValue();

                                getDriverLocation();
                                System.out.println("inhere"+ dataSnapshot.child("fName"));
                                Intent myintent= new Intent(c, RequestBlood.class);
                                myintent.putExtra("DonorId",donorFoundID);
                                startActivity(myintent);


                                }
                            }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                if (!donorFound)//iv the driver wasnt found within tthe radius then do a wide search
                {
                    radius++;
                    getClosestDriver();
                }
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener( this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            }else{
                
            }
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        mMap.setMyLocationEnabled(true);

//        // Add a marker in Sydney and move the camera

//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }  LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for(Location location : locationResult.getLocations()){
                if(getApplicationContext()!=null){
                    mlastLocation = location;

                    LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                    System.out.println("hello here at 292 claaback location");
                    DonorAround();
                }
            }
        }
    };



    List<Marker> avalibleDonors = new ArrayList<Marker>();
    private void DonorAround(){
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("DonorsAvalible");
        GeoFire geo= new GeoFire(db);
        GeoQuery gq= geo.queryAtLocation(new GeoLocation(mlastLocation.getLatitude(),mlastLocation.getLongitude()),10000000);
        gq.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                for (Marker ma:avalibleDonors
                     ) {
                    if(ma.getTag().equals(key))
                        return ;
                    
                }
                LatLng donorLoc=  new LatLng(location.latitude,location.longitude);
                Marker donrMark=mMap.addMarker(new MarkerOptions().position(donorLoc));
                donrMark.setTag(key);
                avalibleDonors.add(donrMark);
                System.out.println("donner foungdonne");
            }

            @Override
            public void onKeyExited(String key) {
                for (Marker ma:avalibleDonors
                        ) {
                    if(ma.getTag().equals(key))
                    {
                        ma.remove();
                        avalibleDonors.remove(ma);
                    };

                }
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                for (Marker ma:avalibleDonors
                        ) {
                    if(ma.getTag().equals(key))
                        ma.setPosition(new LatLng(location.latitude, location.longitude));

                }
            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }
    private Marker mDriverMarker;
    private DatabaseReference driverLocationRef;
    private ValueEventListener driverLocationRefListener;
    private void getDriverLocation(){
        driverLocationRef = FirebaseDatabase.getInstance().getReference().child("DonorsAvalible").child(donorFoundID).child("l");
        driverLocationRefListener = driverLocationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() ){
                    List<Object> map = (List<Object>) dataSnapshot.getValue();
                    double locationLat = 0;
                    double locationLng = 0;
                    if(map.get(0) != null){
                        locationLat = Double.parseDouble(map.get(0).toString());
                    }
                    if(map.get(1) != null){
                        locationLng = Double.parseDouble(map.get(1).toString());
                    }
                    LatLng driverLatLng = new LatLng(locationLat,locationLng);
                    if(mDriverMarker != null){
                        mDriverMarker.remove();
                    }
                    Location loc1 = new Location("");
                    loc1.setLatitude(pickupLocation.latitude);
                    loc1.setLongitude(pickupLocation.longitude);

                    Location loc2 = new Location("");
                    loc2.setLatitude(driverLatLng.latitude);
                    loc2.setLongitude(driverLatLng.longitude);


                    mDriverMarker = mMap.addMarker(new MarkerOptions().position(driverLatLng).title("your donor"));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latlong= new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        if(myMarker!=null){
            myMarker.remove();
        }
        mMap.addMarker(new MarkerOptions().position(latlong)).setTitle("Me");
    }
}

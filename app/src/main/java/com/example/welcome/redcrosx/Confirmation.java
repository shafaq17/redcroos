package com.example.welcome.redcrosx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.welcome.redcrosx.Fragments.NotificationFragment;

public class Confirmation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        Button button = (Button) findViewById(R.id.yes);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                finish();
            }
        });

        Button button2 = (Button) findViewById(R.id.no);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                finish();
            }
        });
    }
}

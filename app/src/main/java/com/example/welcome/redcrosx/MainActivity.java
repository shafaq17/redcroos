package com.example.welcome.redcrosx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button but= (Button) findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast st= new Toast(getApplicationContext());
//                st.setDuration(Toast.LENGTH_LONG);
//                //set location of tast
//                st.setGravity(Gravity.CENTER_VERTICAL,0,0);
//                st.setView(getLayoutInflater().inflate(R.layout.toastlayout,null));
//                st.show();
                Intent i= getIntent();
                User a =(User) i.getParcelableExtra("user");
                Toast.makeText(MainActivity.this, a.getEmail()+" "+a.getBloodGroup(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

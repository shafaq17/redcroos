package com.example.welcome.redcrosx;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.provider.Settings;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SignUp extends AppCompatActivity {
    EditText email;
    EditText passwrd;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    Sensor LightSensor;
    private Window window;

    private static final String TAG = "MainActivity";
    int brightness;
Context c;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.splash);

            c=this;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        MobileAds.initialize(this,getString(R.string.banner_ad_unit_id));
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        window = getWindow();

        LightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(LightSensor != null){

            mSensorManager.registerListener(
                    LightSensorListener,
                    LightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);

        }else{
            Toast.makeText(this,"Sensor.TYPE_LIGHT NOT Available",Toast.LENGTH_SHORT).show();
        }
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                Toast.makeText(c,"shaken baby",Toast.LENGTH_SHORT).show();
            }
        });
//            FirebaseAuth.getInstance().signOut();
//        Intent I = new Intent(this, PhoneAuthActivity.class);
//        startActivity(I);
            new CountDownTimer(5000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    SignUp.this.setContentView(R.layout.activity_sign_up);

                    mAdView = (AdView) findViewById(R.id.adView);
                    AdRequest adRequest = new AdRequest.Builder().build();
                    mAdView.loadAd(adRequest);

                    System.out.println("hello");
                    Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
                    myToolbar.setTitle("");
                    setSupportActionBar(myToolbar);
                     email= (EditText) findViewById(R.id.email);
                     passwrd= (EditText) findViewById(R.id.password);
                    passwrd.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                    email.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                if (!isEmailValid(charSequence)){
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        Drawable drawable=getResources().getDrawable(R.drawable.error);
                                        drawable.setBounds(0, 0,drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());

                                        email.setError("invalid email");
                                    }
                                    else{
                                        Drawable drawable=getResources().getDrawable(R.drawable.common_google_signin_btn_text_disabled);
                                        drawable.setBounds(0, 0,drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());
                                        email.setError("invalid email");
                                    }
                                }
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                    TextView myTextView = (TextView) findViewById(R.id.textView3);
                    myTextView.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View V) {
                            Intent myIntent = new Intent(SignUp.this, LogIn.class);
                            System.out.println("inLoginbutton at93");
                            startActivity(myIntent);
                        }
                    });

                    Button button = (Button) findViewById(R.id.SignUpbtn1);
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View V) {
                            Intent myIntent = new Intent(SignUp.this, SignUp2.class);
                            myIntent.putExtra("email",email.getText().toString());
                            myIntent.putExtra("password",passwrd.getText().toString());
                            //startActivity(myIntent);
                            System.out.println(email.getText().toString()+"  "+passwrd.getText().toString());
                            startActivity(myIntent);
                        }
                    });
                }
            }.start();


    }
    public boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener( LightSensorListener,LightSensor,	SensorManager.SENSOR_DELAY_UI);


    }
    private final SensorEventListener LightSensorListener
            = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_LIGHT){

              if(event.values[0]<20){

                  brightness = 30;
                  Settings.System.putInt(c.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,brightness
                  );
                  WindowManager.LayoutParams layoutpars = window.getAttributes();

                  layoutpars.screenBrightness = brightness / (float)255;

                  window.setAttributes(layoutpars);

                  Toast.makeText(c,"Light is low!",Toast.LENGTH_SHORT).show();
              }
              else{
                  brightness = 120;
                  Settings.System.putInt(c.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,brightness
                  );
                  WindowManager.LayoutParams layoutpars = window.getAttributes();

                  layoutpars.screenBrightness = brightness / (float)255;

                  window.setAttributes(layoutpars);

              }
            }
        }

    };

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        mSensorManager.unregisterListener(LightSensorListener);
        super.onPause();
    }
}

package com.example.welcome.redcrosx;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.welcome.redcrosx.Tabs.MyAdapter;
import com.example.welcome.redcrosx.Tabs.SlidingTabLayout;

public class ProfileTabs extends AppCompatActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_tabs);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar5);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);

        viewPager =(ViewPager)findViewById(R.id.vp_tabs);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager(),this));

        mSlidingTabLayout=(SlidingTabLayout)findViewById(R.id.stl_tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view,R.id.tv_tab);
        mSlidingTabLayout.setViewPager(viewPager);
        //ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
       // setupViewPager(viewPager);
    }
}

package com.example.welcome.redcrosx.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.welcome.redcrosx.ProfileOption.CaptureMoment;
import com.example.welcome.redcrosx.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {

    private ImageButton btnSpeak;
    EditText status;
    FirebaseDatabase myDb;
    private TextView editText;
    Button post;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile, container, false);
        btnSpeak = (ImageButton) view.findViewById(R.id.imgbtn);
        editText = (TextView) view.findViewById(R.id.textView13);
editText.setVisibility(View.VISIBLE);

        status=(EditText) view.findViewById(R.id.statusText);
        status.setVisibility(View.INVISIBLE);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                status.setVisibility(View.VISIBLE);
                editText.setVisibility(View.INVISIBLE);
                post.setVisibility(View.VISIBLE);
                promptSpeechInput();
           }
        });
        DatabaseReference ref=FirebaseDatabase.getInstance().getReference().child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("status")!=null){
                    System.out.println(dataSnapshot.child("status"));
                    editText.setText(dataSnapshot.child("status").getValue(String.class));
                }
                else {
                    editText.setText("whats on your mind? ");


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        post=(Button)view.findViewById(R.id.postMe);
        post.setVisibility(View.INVISIBLE);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myDb= FirebaseDatabase.getInstance();
                DatabaseReference ref=myDb.getReference().child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("status");
                ref.setValue(status.getText().toString());
                ref.keepSynced(true);
                status.setVisibility(View.INVISIBLE);
                editText.setVisibility(View.VISIBLE);
                post.setVisibility(View.INVISIBLE);

            }
        });

        Button button = (Button) view.findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                Intent I = new Intent(getActivity(), CaptureMoment.class);
                startActivity(I);
            }
        });

        return view;
    }

    public void promptSpeechInput()
    {
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.ENGLISH);
        i.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say Something");
        try {
            startActivityForResult(i, 100);
        }catch(ActivityNotFoundException e)
        {
            Toast.makeText(getActivity(),"Sorry your device doesn't support speech language",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case 100:
                if(resultCode == RESULT_OK && data !=null)
                {
                    ArrayList<String> result= data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    status.setText(result.get(0));

                }
                break;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

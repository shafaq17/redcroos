package com.example.welcome.redcrosx.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.welcome.redcrosx.R;

import java.util.ArrayList;

/**
 * Created by Khalid on 4/22/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    // The items to display in your RecyclerView
    private ArrayList <Notification> items;
    private Context c;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapter(ArrayList<Notification> items, Context context) {
        this.items = items;
        this.c = context;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getFlag();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.notification_view, viewGroup, false);
                viewHolder = new MyViewHolder(v1);
                break;
            case 1:
                View v2 = inflater.inflate(R.layout.contacts_view, viewGroup, false);
                viewHolder = new MyViewHolder2(v2);
               break;
            case 2:
                View v3 = inflater.inflate(R.layout.history_view, viewGroup, false);
                viewHolder = new MyViewHolder3(v3);
                break;
            default:
                View v = inflater.inflate(R.layout.empty_message_view, viewGroup, false);
                viewHolder = new MyViewHolder(v);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case 0:
                ((MyViewHolder) viewHolder).bindView(position);
                break;
            case 1:
                ((MyViewHolder2) viewHolder).bindView(position);
                break;
            case 2:
                 ((MyViewHolder3) viewHolder).bindView(position);
                 break;
            default:
                ((MyViewHolder) viewHolder).bindView(position);
                break;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView Name;
        TextView DateTime;
        TextView Request;
        TextView Confirm;
        ImageView Dp;
        ImageView Arrow;

        public MyViewHolder(View itemview) {
            super(itemview);
            Name = (TextView) itemView.findViewById(R.id.name);
            DateTime = (TextView) itemView.findViewById(R.id.datetime);
            Request = (TextView) itemView.findViewById(R.id.Request);
            Confirm = (TextView) itemView.findViewById(R.id.Confirm);
            Dp = (ImageView) itemView.findViewById(R.id.Dp);
            Arrow = (ImageView) itemView.findViewById(R.id.imageView3);
        }

        public void bindView(final int position) {
                Name.setText(items.get(position).getRecipentName());
                DateTime.setText(items.get(position).getNotificationDate());
                Request.setText(items.get(position).getMessage());
                Confirm.setText("please confirm?");
                Dp.setImageResource(R.drawable.logo);
                Arrow.setImageResource(R.drawable.arrow);
            }

        public void onClick(View view) {

        }
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView Name;
        TextView Request;
        TextView Confirm;
        ImageView Dp;
        ImageView Arrow;

        public MyViewHolder2(View itemview) {
            super(itemview);
            Name = (TextView) itemView.findViewById(R.id.name);
            Request = (TextView) itemView.findViewById(R.id.Request);
            Confirm = (TextView) itemView.findViewById(R.id.Confirm);
            Dp = (ImageView) itemView.findViewById(R.id.Dp);
            Arrow = (ImageView) itemView.findViewById(R.id.imageView3);
        }

        public void bindView(final int position) {

            Name.setText(items.get(position).getRecipentName());
            Request.setText(items.get(position).getMessage());
            Dp.setImageResource(R.drawable.logo);
            Confirm.setText(items.get(position).getAppointment());
            Arrow.setImageResource(R.drawable.arrow);
        }

        public void onClick(View view) {

        }
    }

    public class MyViewHolder3 extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView Name;
        TextView DateTime;
        TextView Request;
        ImageView Dp;
        ImageView Arrow;

        public MyViewHolder3(View itemview) {
            super(itemview);
            Name = (TextView) itemView.findViewById(R.id.name);
            DateTime = (TextView) itemView.findViewById(R.id.datetime);
            Request = (TextView) itemView.findViewById(R.id.Request);
            Dp = (ImageView) itemView.findViewById(R.id.Dp);
            Arrow = (ImageView) itemView.findViewById(R.id.imageView3);
        }

        public void bindView(final int position) {

            Name.setText(items.get(position).getRecipentName());
            DateTime.setText(items.get(position).getNotificationDate());
            Request.setText(items.get(position).getMessage());
            Dp.setImageResource(R.drawable.logo);
            Arrow.setImageResource(R.drawable.arrow);
        }

        public void onClick(View view) {

        }
    }

}

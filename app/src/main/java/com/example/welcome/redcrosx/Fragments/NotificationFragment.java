package com.example.welcome.redcrosx.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.welcome.redcrosx.Confirmation;
import com.example.welcome.redcrosx.ProfileOption.CaptureMoment;
import com.example.welcome.redcrosx.RecyclerView.Notification;
import com.example.welcome.redcrosx.R;
import com.example.welcome.redcrosx.RecyclerView.RecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

import static android.support.constraint.Constraints.TAG;

public class NotificationFragment extends Fragment{

    private FirebaseDatabase mFirebaseDatabase;
    RecyclerAdapter mydapter;
    private DatabaseReference mnotificationref;
    private ChildEventListener mChildEventListener;
    TextToSpeech texttospeech;
    ArrayList<Notification> items= new ArrayList<>();

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        items.clear();
        Notification n1=new Notification();
        n1.setFlag(0);
        n1.setRecipentName("Shafaq");
        n1.setNotificationDate("April 25, 9:30 am");
        n1.setReciepientId("fdssdf");
        n1.setMessage("Requested A+ blood");
        n1.setMid("98");
        items.add(n1);
        View view=inflater.inflate(R.layout.fragment_notification, container, false);
        texttospeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if(i == TextToSpeech.SUCCESS)
                {
                    int result= texttospeech.setLanguage(Locale.ENGLISH);
                    if(result == TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED)
                    {
                        Toast.makeText(getActivity(),"This Language is not Supportted",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        texttospeech.setPitch(0.6f);
                        texttospeech.setSpeechRate(1.0f);
                        speak("");
                    }
                }
            }
        });
            RecyclerView recycleview = (RecyclerView) view.findViewById(R.id.list);
            recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycleview.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                    recycleview, new ClickListener() {

                @Override
                public void onClick(View view, final int position)
                {
                    Toast.makeText(getActivity(),"Single Click on position :"+position,Toast.LENGTH_SHORT).show();
                    String text=items.get(position).getRecipentName()+items.get(position).getMessage()+"on"+items.get(position).getNotificationDate()+". Please confirm?";
                    speak(text);
                    Intent I = new Intent(getActivity(), Confirmation.class);
                    startActivity(I);

            }
            }));
            mydapter=new RecyclerAdapter(items, getActivity());
            recycleview.setAdapter(mydapter);
              System.out.println("here");

            return view;
        }

        public static interface ClickListener{
            public void onClick(View view,int position);
        }

        class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

            private ClickListener clicklistener;
            private GestureDetector gestureDetector;

            public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

                this.clicklistener=clicklistener;
                gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener()
                {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });
            }

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child=rv.findChildViewUnder(e.getX(),e.getY());
                if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                    clicklistener.onClick(child,rv.getChildAdapterPosition(child));
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
        }
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mnotificationref = mFirebaseDatabase.getReference().child("notification").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mnotificationref.keepSynced(true);
        mnotificationref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    System.out.println(postSnapshot.getKey());
                    DatabaseReference my=mnotificationref.child(postSnapshot.getKey());
                    Notification myNot=postSnapshot.getValue(Notification.class);
                    myNot.setMid(postSnapshot.getKey());
                    check(myNot);

                    items.add(myNot);
                    mydapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(),myNot.getMessage(),Toast.LENGTH_LONG).show();
                    System.out.println("helO"+myNot.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        });

    }

    private void check(Notification myNot) {
        for(int i=0;i<items.size();i++){
            if(items.get(i).getMid().equals(myNot.getMid())){
                items.remove(i);
                mydapter.notifyDataSetChanged();
            }
        }
    }

    private void speak(String t) {
        String text=t;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            texttospeech.speak(text,TextToSpeech.QUEUE_FLUSH,null,null);
        }
        else
        {
            texttospeech.speak(text,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    @Override
    public void onDestroy() {
        if(texttospeech != null)
        {
            texttospeech.stop();
            texttospeech.shutdown();
        }
        super.onDestroy();
    }
}

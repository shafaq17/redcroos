package com.example.welcome.redcrosx.RecyclerView;

/**
 * Created by shafaq on 4/23/2018.
 */

public class Notification {
    String appointment;
    int flag;
    String message;
    String notificationDate;
    String recipentName;
    String reciepientId;
    String mid;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getRecipentName() {
        return recipentName;
    }

    public void setRecipentName(String recipentName) {
        this.recipentName = recipentName;
    }

    public String getReciepientId() {
        return reciepientId;
    }

    public void setReciepientId(String reciepientId) {
        this.reciepientId = reciepientId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }
}

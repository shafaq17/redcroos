package com.example.welcome.redcrosx.ProfileOption;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.welcome.redcrosx.R;
import com.example.welcome.redcrosx.Twitter.WebViewActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.ByteArrayOutputStream;

public class CaptureMoment extends AppCompatActivity {
    private static final String TAG = "fb";

    TwitterLoginButton LoginButton;
    Bitmap image;
    ImageView img;
    Context c;
    ShareButton share;
    CallbackManager  callbackManager;
    ShareDialog shareDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
        setContentView(R.layout.activity_capture_moment);
        c=this;
        share = (ShareButton)findViewById(R.id.fb_share_button);
        Button capture= (Button)findViewById(R.id.button7);
        capture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                Intent Camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(Camera,10);
            }
        });
        img = (ImageView)findViewById(R.id.imageView5);

        LoginButton = (TwitterLoginButton) findViewById(R.id.twitterlogin);
        LoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session= TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken=session.getAuthToken();
                String token =authToken.token;
                String secret = authToken.secret;
                login(session);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(CaptureMoment.this,"Failed",Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void login(TwitterSession session)
    {
        String username = session.getUserName();
        Intent i = new Intent(CaptureMoment.this, WebViewActivity.class);
        i.putExtra("username",username);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if (requestCode == 10) {
                 image = (Bitmap) data.getExtras().get("data");
                img.setImageBitmap(image);
                share();

            }
            else
            {
                LoginButton.onActivityResult(requestCode,resultCode,data);
            }
        }


    }
    public  void share(){
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        share.setShareContent(content);

    }
}

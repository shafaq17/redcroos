package com.example.welcome.redcrosx.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.welcome.redcrosx.RecyclerView.Notification;
import com.example.welcome.redcrosx.R;
import com.example.welcome.redcrosx.RecyclerView.RecyclerAdapter;

import java.util.ArrayList;

public class HistoryFragment extends Fragment {

    public HistoryFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ArrayList<Notification> items= new ArrayList<>();
        Notification n1=new Notification();
        n1.setFlag(2);
        n1.setRecipentName("Shafaq");
        n1.setNotificationDate("April 25 9:30 am");
        n1.setReciepientId("fdssdf");
        n1.setMessage("Donated A+ blood");
        items.add(n1);
        View view=inflater.inflate(R.layout.fragment_history, container, false);
        RecyclerView recycleview = (RecyclerView) view.findViewById(R.id.list);
        recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleview.addOnItemTouchListener(new HistoryFragment.RecyclerTouchListener(getActivity(),
                recycleview, new NotificationFragment.ClickListener() {

            @Override
            public void onClick(View view, final int position)
            {
                Toast.makeText(getActivity(),"Single Click on position :"+position,Toast.LENGTH_SHORT).show();
            }
        }));
        recycleview.setAdapter(new RecyclerAdapter(items, getActivity()));

        return view;
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private NotificationFragment.ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final NotificationFragment.ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
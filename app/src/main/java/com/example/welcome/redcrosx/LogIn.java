package com.example.welcome.redcrosx;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;

public class LogIn extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    public Context c;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG1 = "PhoneAuthActivity";
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
    public void SetUserLocation(Location i){
        DatabaseReference myrec=FirebaseDatabase.getInstance().getReference("users");

        System.out.println("hello ji girl");

//        myrec.child("email").equalTo(FirebaseAuth.getInstance().getCurrentUser().getEmail()).addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
//                System.out.println("val is"+dataSnapshot.child("user").child("email").getValue());
//                System.out.println("val is"+dataSnapshot.child("email").getValue());
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                System.out.println("val is"+dataSnapshot.child("user").child("email").getValue());
//                System.out.println("val is"+dataSnapshot.child("email").getValue());
//                //ystem.out.println("val is"+dataSnapshot.child("user").child("email").getValue());
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//
//
//        });
    }
    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    LoginButton fb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar2);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);
        System.out.println("hefre");
        TextView myTextView = (TextView) findViewById(R.id.textView6);
        myTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent myIntent = new Intent(LogIn.this, SignUp.class);
                startActivity(myIntent);
            }
        });
        final TextView email=(TextView) findViewById(R.id.email2);
        final TextView password=(TextView)findViewById(R.id.password);
// Initi
        callbackManager = CallbackManager.Factory.create();


        fb = (LoginButton) findViewById(R.id.login_button);
        fb.setReadPermissions(Arrays.asList(EMAIL));
        fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("fblogin", "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d("dblogoin", "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("dblogoin", "facebook:onError", error);
                // ...
            }
        });


        Button button = (Button) findViewById(R.id.LogInbtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                signInWithEmail(email.getText().toString(),password.getText().toString());
            }
        });

        Button smsSignIn = (Button) findViewById(R.id.SMS);
        smsSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I = new Intent(c, PhoneAuthActivity.class);
                startActivity(I);
            }
        });
        googleSignInThing();
    }
    private void signInWithEmail(String email,String password) {


        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            Intent myIntent = new Intent(LogIn.this, MapsActivity.class);
                            startActivity(myIntent);

                        } else {
                            Toast.makeText(LogIn.this, "Sign In Failed",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void googleSignInThing() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        //System.out.println("211944222004-cbrkokjptaoral7h6cfklpm4di47m5ls.apps.googleusercontent.com");
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso).build()
        ;

        mAuth=FirebaseAuth.getInstance();
        SignInButton b=(SignInButton) findViewById(R.id.sign_in_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        c=this;
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user=firebaseAuth.getCurrentUser();
                if(user!=null) {
                    //user is signed in
                    Intent i = new Intent(c, MapsActivity.class);
                    startActivity(i);
                }
            }
        };

    }
    protected void onStart() {
        super.onStart();
        if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            Intent i =new Intent(this,MapsActivity.class);
            startActivity(i);
        }
        System.out.println("loginstart at93");
        mAuth.addAuthStateListener(mAuthListener);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            GoogleSignInResult rs=Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            GoogleSignInAccount account = null;
            if(rs.isSuccess()){
                Log.e("sycce","succes");
                account =  rs.getSignInAccount();
                firebaseAuthWithGoogle(account);
            }
            else
                updateUI1(account);
        }
        else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("fb", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("fb", "signInWithCredential:success");

                            FirebaseUser user = mAuth.getCurrentUser();
                            User anew=new User();
                            anew.setEmail(user.getEmail());
                            anew.setPassword(null);
                            anew.setLname(user.getDisplayName().split(" ")[1]);
                            anew.setfName(user.getDisplayName().split(" ")[0]);
                            anew.setBloodGroup("not avalible");
                            FirebaseDatabase.getInstance().getReference().child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(anew);
                            updateUI(user);
                            System.out.println("hello");
                            Intent myIntent = new Intent(c, MapsActivity.class);
                            startActivity(myIntent);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("fb", "signInWithCredential:failure", task.getException());
                            Toast.makeText(c, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("err","jsiss");
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("erro", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            User anew=new User();
                            anew.setEmail(user.getEmail());
                            anew.setPassword(null);
                            anew.setLname(user.getDisplayName().split(" ")[1]);
                            anew.setfName(user.getDisplayName().split(" ")[0]);
                            anew.setBloodGroup("not avalible");
                            FirebaseDatabase.getInstance().getReference().child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(anew);
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(anew.getfName()+" "+anew.getLname()).build();

                            FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates);
                            updateUI(user);
                            Intent myIntent = new Intent(c, MapsActivity.class);
                            startActivity(myIntent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("erro", "signInWithCredential:failure", task.getException());
                            Toast.makeText(c, "Authentication Failed.", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }


                        // [END_EXCLUDE]
                    }

                });
    }

    private void updateUI(FirebaseUser user) {
        if(user!=null){
            Toast.makeText(c,user.getEmail(),Toast.LENGTH_LONG).show();
        }
    }



    private void updateUI1(GoogleSignInAccount account) {
        if(account!=null){
            firebaseAuthWithGoogle(account);
            Toast.makeText(c,account.getEmail(),Toast.LENGTH_LONG).show();
        }

        Log.e("hs","ey");
    }

    private void signIn() {
        /*Starting the intent prompts the user to select a Google account to sign in with.
         If you requested scopes beyond profile, email, and openid, the user is also prompted to grant access to the requested resources.

         */
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
        Log.e("er","err");
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

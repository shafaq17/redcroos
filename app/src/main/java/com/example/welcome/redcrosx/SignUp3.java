package com.example.welcome.redcrosx;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUp3 extends AppCompatActivity {
    public EditText BloodGroup;
    private FirebaseDatabase myDb;
    FirebaseAuth a;
    public SharedPreferences myshared;
    private DatabaseReference usertable;//referes to user portion of db
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up3);
        a = FirebaseAuth.getInstance();
        Log.e("hello","here");
        myDb= FirebaseDatabase.getInstance();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Log.e("hello","here");
        usertable=myDb.getReference().child("users");
        System.out.println("here");
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar4);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);
        BloodGroup =(EditText) findViewById(R.id.blood);
        Button button = (Button) findViewById(R.id.SignUpbtn3);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                String Bgroup=BloodGroup.getText().toString();
                if(!TextUtils.isEmpty(BloodGroup.getText().toString())){
                    if(isValidBlood(Bgroup)){

                        User anew=new User();
                        anew.setEmail(getIntent().getStringExtra("email1"));
                        anew.setPassword(getIntent().getStringExtra("password1"));
                        anew.setLname(getIntent().getStringExtra("lname"));
                        anew.setfName(getIntent().getStringExtra("fname"));
                        anew.setBloodGroup(Bgroup);

                        a.createUserWithEmailAndPassword(anew.getEmail(),anew.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(SignUp3.this, "User addecd.",
                                            Toast.LENGTH_SHORT).show();
                                    User anew=new User();
                                    anew.setEmail(getIntent().getStringExtra("email1"));
                                    anew.setPassword(getIntent().getStringExtra("password1"));
                                    anew.setLname(getIntent().getStringExtra("lname"));
                                    anew.setfName(getIntent().getStringExtra("fname"));
                                    anew.setBloodGroup(BloodGroup.getText().toString());
                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(anew.getfName()+" "+anew.getLname()).build();

                                    FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates);
                                    usertable.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(anew);
                                    Intent myIntent = new Intent(SignUp3.this, LogIn.class);
                                    startActivity(myIntent);

                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.e("error",task.getException().getMessage());
                                    Toast.makeText(SignUp3.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }

                            }
                        });

                    }
                }
            }
        });
    }

    private boolean isValidBlood(String bgroup) {
        return true;
    }
}

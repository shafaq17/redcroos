package com.example.welcome.redcrosx;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Welcome on 4/4/2018.
 */
//
public class User implements Parcelable{
    String fName;
    String lname;
    String email;
    String password;
    String bloodGroup;
    public  User(){
      fName="shafaq";
      lname="arshad";
        email="arshad.shafaq15@gmail.com";
        password="1234";
        bloodGroup="A+";
    }
    public User(String fName,String lname, String email, String password, String bloodGroup) {
        this.fName = fName;
        this.lname=lname;
        this.email = email;
        this.password = password;
        this.bloodGroup = bloodGroup;
    }



    protected User(Parcel in) {
        fName = in.readString();
        lname = in.readString();
        email = in.readString();
        password = in.readString();
        bloodGroup = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName=fName;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fName);
        parcel.writeString(lname);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(bloodGroup);

    }
}

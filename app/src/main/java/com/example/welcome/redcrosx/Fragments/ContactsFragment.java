package com.example.welcome.redcrosx.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.welcome.redcrosx.RecyclerView.Notification;
import com.example.welcome.redcrosx.R;
import com.example.welcome.redcrosx.RecyclerView.RecyclerAdapter;
import com.google.android.gms.appinvite.AppInviteInvitation;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

public class ContactsFragment extends Fragment {

    public ContactsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ArrayList<Notification> items= new ArrayList<>();
        Notification n1=new Notification();
        n1.setFlag(1);
        n1.setRecipentName("aqsa");
        n1.setAppointment("I am a happy person!!!");
        n1.setReciepientId("fdssdf");
        n1.setMessage("111-222-333");
        items.add(n1);
        View view=inflater.inflate(R.layout.fragment_contacts, container, false);
        Button invite=(Button) view.findViewById(R.id.button3);
        invite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                try {
                    Intent intent = new AppInviteInvitation.IntentBuilder("Red Cross")
                            .setMessage("Donate Blood, Save a Life")
                               .setDeepLink(Uri.parse("https://play.google.com/store/apps/details?id=com.cube.arc.blood&utm_source=google_play&utm_medium=web&utm_campaign=blood_donor_app_google"))
                               .setCustomImage(Uri.parse("https://www.google.com.pk/search?q=blood+drop+image&safe=active&rlz=1C1CHBD_enPK792PK793&tbm=isch&source=iu&ictx=1&fir=9VcoahFwyDAFfM%253A%252CM2vBPp04sjrcVM%252C_&usg=__EI3hvo2cVSo_2km3xvHHMDNGoC8%3D&sa=X&ved=0ahUKEwiop7S3peDaAhVDPRQKHbxAC50Q9QEIKDAA#imgrc=F_5e3xMeYoI5vM:"))
                               .setCallToActionText("Install!!!")
                            .build();
                    Toast.makeText(getActivity(),"Invite Sending",Toast.LENGTH_SHORT).show();
                    startActivityForResult(intent, 1);
                } catch (ActivityNotFoundException ac) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Red Cross");
                    sendIntent.setType("text/plain");
                    Toast.makeText(getActivity(),"Exception",Toast.LENGTH_SHORT).show();
                    startActivity(sendIntent);
                }
            }
        });
        RecyclerView recycleview = (RecyclerView) view.findViewById(R.id.list);
        recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleview.addOnItemTouchListener(new ContactsFragment.RecyclerTouchListener(getActivity(),
                recycleview, new NotificationFragment.ClickListener() {

            @Override
            public void onClick(View view, final int position)
            {
                Toast.makeText(getActivity(),"Single Click on position :"+position,Toast.LENGTH_SHORT).show();
            }
        }));
        recycleview.setAdapter(new RecyclerAdapter(items, getActivity()));

        return view;
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private NotificationFragment.ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final NotificationFragment.ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                Log.d(TAG, "Invitation Sent");
                Toast.makeText(getActivity(),"All good",Toast.LENGTH_SHORT).show();
            } else {

                Log.d(TAG, "invite send failed or cancelled:" + requestCode + ",resultCode:" + resultCode );
                Toast.makeText(getActivity(),"Problem",Toast.LENGTH_SHORT).show();
            }
        }
    }

}

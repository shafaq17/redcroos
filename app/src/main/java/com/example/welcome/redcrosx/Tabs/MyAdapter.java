package com.example.welcome.redcrosx.Tabs;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.example.welcome.redcrosx.Fragments.ContactsFragment;
import com.example.welcome.redcrosx.Fragments.HistoryFragment;
import com.example.welcome.redcrosx.Fragments.NotificationFragment;
import com.example.welcome.redcrosx.Fragments.ProfileFragment;
import com.example.welcome.redcrosx.R;

/**
 * Created by Khalid on 4/20/2018.
 */

public class MyAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private Context mcontext;
    private String[] Titles={"A","B","C","D"};
    int[] icon=new int[]{R.drawable.wprofile,R.drawable.wcontact,R.drawable.wnotification,R.drawable.whistory};
    private int heighticon;

    public MyAdapter(FragmentManager fm, Context c) {
        super(fm);
        mcontext=c;
        double scale =c.getResources().getDisplayMetrics().density;
        heighticon=(int)(24*scale+0.5f);
    }

    @Override
    public int getCount()
    {
        return Titles.length;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment frag=null;
        if(position == 0)
        {
            frag=new ProfileFragment();
        }
        else if(position == 1)
        {
            frag=new ContactsFragment();
        }
        else if(position == 2)
        {
            System.out.println("here");
            frag=new NotificationFragment();
        }
        else if(position == 3)
        {
            frag=new HistoryFragment();
        }

        Bundle b=new Bundle();
        b.putInt("Position",position);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        Drawable d = mcontext.getResources().getDrawable(icon[position]);
        d.setBounds(0,0,heighticon,heighticon);
        ImageSpan is=new ImageSpan(d);

        SpannableString sp=new SpannableString(" ");
        sp.setSpan(is,0,sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sp;
    }
}
